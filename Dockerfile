FROM python:buster as base

FROM base as builder

RUN apt-get update

RUN mkdir /install
WORKDIR /install

COPY src/requirements.txt /requirements.txt

RUN pip install --target=/install -r /requirements.txt

FROM base

COPY --from=builder /install /usr/local

CMD mkdir /opt/ml/code
CMD mkdir /opt/ml/data

COPY src /opt/ml/code

WORKDIR /opt/ml/code

CMD ["python", "entrypoint.py"]
