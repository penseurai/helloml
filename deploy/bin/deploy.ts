#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { DeployStack } from '../lib/deploy-stack';

const app = new cdk.App();
new DeployStack(app, 'DeployStack',
{
        repositoryName: "aws-cdk/assets",
        imageTag: "myImage",
        scheduleExpression: "rate(1 hour)"
});
