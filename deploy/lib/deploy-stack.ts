import * as cdk from '@aws-cdk/core';
import * as s3 from '@aws-cdk/aws-s3';
import * as ec2 from '@aws-cdk/aws-ec2';
import * as ecs from '@aws-cdk/aws-ecs';
import * as ecr from '@aws-cdk/aws-ecr';
import { DockerImageAsset } from '@aws-cdk/aws-ecr-assets';
import * as ecs_patterns from '@aws-cdk/aws-ecs-patterns';
import {Schedule} from '@aws-cdk/aws-events';
import { InstanceType, NatInstanceProvider, NatProvider, InstanceClass, InstanceSize } from '@aws-cdk/aws-ec2';

interface DeployStackProps extends cdk.StackProps {
  repositoryName: string,
  imageTag: string,
  scheduleExpression: string
}

export class DeployStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props: DeployStackProps) {
    super(scope, id, props);

    // here you should specify the VPC that you want to use
    // In this example the default vpc is querried for, but 
    const vpc = ec2.Vpc.fromLookup(this, 'DefaultVPC', {
      isDefault: true
    });   

    // get the repository with a particular name
    //
    const repository = ecr.Repository.fromRepositoryName(
      this, 'Repository', props.repositoryName);

    // find an image from the repository specified with the tag
    // this is the tag that we used when pushing the image to ECR 
    // after building the Docker image
    const containerImage = ecs.ContainerImage.fromEcrRepository(
      repository, props.imageTag
    )

    // Create an ECS cluster
    const cluster = new ecs.Cluster(this, 'Cluster', {
      vpc,
    });

    // Instantiate an Amazon EC2 Task to run at a scheduled interval
    const ecsScheduledTask = new ecs_patterns.ScheduledEc2Task(this, 'ScheduledTask', {
      cluster,
      scheduledEc2TaskImageOptions: {
        image: containerImage,
        memoryLimitMiB: 256,
        environment: { name: 'TRIGGER', value: 'CloudWatch Events' },
      },
      schedule: Schedule.expression( props.scheduleExpression)
    });  
  }
}
