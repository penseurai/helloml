import * as cdk from '@aws-cdk/core';
import * as s3 from '@aws-cdk/aws-s3';
import * as ec2 from '@aws-cdk/aws-ec2';
import * as ecs from '@aws-cdk/aws-ecs';
import { DockerImageAsset } from '@aws-cdk/aws-ecr-assets';
import * as ecs_patterns from '@aws-cdk/aws-ecs-patterns';
import {Schedule} from '@aws-cdk/aws-events';
import { InstanceType, NatInstanceProvider, NatProvider, InstanceClass, InstanceSize } from '@aws-cdk/aws-ec2';

export class HelloMLDeployStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // here you should specify the VPC that you want to use
    // In this example the default vpc is querried for, but 
    const vpc = ec2.Vpc.fromLookup(this, 'DefaultVPC', {
      isDefault: true
    });   

    const asset = new DockerImageAsset(this, 'HelloMlImage', {
      directory: "../../",
      file: "Dockerfile"
    });    

    // Create an ECS cluster
    const cluster = new ecs.Cluster(this, 'Cluster', {
      vpc,
    });

    // Instantiate an Amazon EC2 Task to run at a scheduled interval
    const ecsScheduledTask = new ecs_patterns.ScheduledEc2Task(this, 'ScheduledTask', {
      cluster,
      scheduledEc2TaskImageOptions: {
        image: ecs.ContainerImage.fromRegistry(asset.imageUri),
        memoryLimitMiB: 256,
        environment: { name: 'TRIGGER', value: 'CloudWatch Events' },
      },
      schedule: Schedule.expression('rate(1 hour)')
    });
  }
}
