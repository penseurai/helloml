# GitLab Multi-stage Template

The purpose of this template is to provide a mechanism for handling the two components of a deployment of a Docker/Fargate project. 

In the first phase of the deployment, the project will build a Docker image based on the contents of the included Docker file. 
It will then publish the resulting Docker image to Amazon ECR. 

In the second phase of the deployment, the project will utilize the AmazonCDK library to use constructs to publish the image to a scheduled 
task. This could be, effectively, any sort of operation - but for the purposes of this project the second phase will get a Vpc, setup an ECS
cluster in the VPC, deploy the Docker image from the ECR registry in the first phase to the cluster and schedule that Docker image
to run at an interval at its entrypoint.

This project was designed specifically to handle this using gitlab and run on gitlab runners utilizing Docker-in-Docker approaches.

