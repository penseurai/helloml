from setuptools import setup

setup(
    name='helloml',
    entry_points={
        'console_scripts': [
            'helloml = entrypoint:main',
        ],
    }
)
